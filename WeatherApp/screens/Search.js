import React, { useState } from 'react';
import {
    View, Text, FlatList
} from 'react-native';
import { TextInput, Button, Card } from 'react-native-paper';
import AsyncStorage from "@react-native-community/async-storage"

import Header from './Header';



const Search = ({ navigation }) => {
    const [city, setCity] = useState('')
    const [cities, setCities] = useState([])
    const fetchCities = (text) => {
        setCity(text)
        fetch("http://ec2-54-197-242-142.compute-1.amazonaws.com:8080/otp/routers/default/geocode?query=" + text)
            .then(item => item.json())
            .then(cityData => {
                setCities(cityData)
                // console.log(cityData)
            })
    }

    const btnClick = async () => {
        await AsyncStorage.setItem("newcity", city)
        navigation.navigate("Home", { city: city })
    }

    const listClick = async (cityname) => {
        setCity(cityname)
        await AsyncStorage.setItem("newcity", cityname)
        navigation.navigate("Home", { city: cityname })
    }


    return (
        <View style={{ flex: 1 }}>
            <Header name="Search Screen" />
            <TextInput
                lable="City name"
                theme={{ colors: { primary: "#00aaff" } }}
                value={city}
                onChangeText={(text) => fetchCities(text)} />

            <Button
                // icon="content-save"
                mode="contained"
                theme={{ colors: { primary: "#00aaff" } }}
                style={{ margin: 20 }}
                onPress={() => btnClick()}>
                <Text style={{ color: "white" }}>Save Changes</Text>
            </Button>

            <FlatList
                data={cities}
                renderItem={({ item }) => {
                    console.log(item)
                    return (
                        <Card
                            style={{ margin: 2, padding: 12 }}
                            onPress={() => listClick(item.description)}
                        >
                            <Text>{item.description}</Text>
                        </Card>
                    )
                }}
                keyExtractor={item => item.name}
            />
        </View>
    );
};



export default Search;
