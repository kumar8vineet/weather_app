import React from 'react';
import {
    View,
    Text,
    StatusBar,
} from 'react-native';
import { Appbar, Title } from 'react-native-paper';


const Header = (props) => {
    return (
        <Appbar.Header theme={{
            colors: {
                primary: "#00aaff"
            }
        }} style={{ flexDirection: "row", justifyContent: "center" }}>
            <Title style={{ color: "white" }}>
                {props.name}
            </Title>
        </Appbar.Header>
    );
};



export default Header;
